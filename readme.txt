# Установка

Установить nodejs
Запустить install.bat
Установить билиотеку graphics magic http://www.graphicsmagick.org/INSTALL-windows.html
Перезагрузить комп
Открыть консоль, ввести команду gm, должен отобразиться хелп. Если команда не найдена. Значит graphics magic не установился

# Запуск

Запустить start.bat
открыть в браузере localhost:3000
На планшете http://ip_computera:3000 сделать ярлык на домашнем экране

# Настройки
1. В settings/config.json можно менять имя отправителя и тему письма. Поля - from, subject
2. В static/logo.png - заменяется лого
3. Рамка меняется в папке settings/frame.png Это png с прозрачностью 900х600 минимум, чем больше тем лучше

# Печать
"send_to_email": true/false - вкл выкл отправки в почту
"send_to_print": true/false - вкл выкл печати
"printer_name": "Microsoft Print to PDF" - задать имя принтера

# Отслеживание фотографий
По умолчанию отлеживается все что в папке photos. Как только появляется новый файл, делается превью и он появляется в сервере.
Путь к папке отслеживания можно поменять в конфиге. Параметр watchpath - путь к папке отслеживания фото. Без слешей в начале и в конце. Относительно корневой папки проекта. Если нужно отселживать папку на уровень выше, прописать относительный путь "../foldername"

# Сброс данных
Удалить все файлы в папках db, photos, static/images/standrd/, static/images/thumbnails. Сами папки не стирать.

# Админка

http://localhost:3000/admin
Пароль - 123

В админке можно смотреть статистику, статус отправки фотографий.