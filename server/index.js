var nodemailer = require('nodemailer');
var Datastore = require('nedb')
var express = require('express');
var config = require('../settings/config');
var hound = require('hound');
var exec = require('child_process').exec;
var http = require('http');
var path = require('path');
var WS = require('ws');
var gm = require('gm');
var fs = require('fs');


// DB
var dbContacts = new Datastore({filename: 'db/contacts.db', autoload: true});
var dbPhoto = new Datastore({filename: 'db/photo.db', autoload: true});
 
dbPhoto.find({}).exec(function (err, docs) {
    count = docs.length || 0;
    console.log('Найдено ' + count + ' фотографий')
});

// DIRS

var dirs = [config.watchpath,'static/images/', 'static/images/standard','static/images/thumbnail'];

dirs.forEach(function(dir){
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
});


// MAIL
var html = fs.readFileSync(__dirname + '/mail.html').toString();

var transporter = nodemailer.createTransport(config.mail.smtp);
 
sendMail = function(data){
    if(data.attempts < (config.attempts_limit || 100) && !data.sent && !data.print){ 
        var replase = function(str){
            return str.replace(/%([a-zA-Z1-9_]+?)%/gm, function(mayh, key){
                return data[key]
            });
        }

        if(!data.sent){  
            var option = {
                from: config.mail.from, 
                to: data.email,
                subject: config.mail.subject,
                text: replase(config.mail.text || html),
                html: replase(config.mail.html || html),
                attachments: []
            }
            data.photos.forEach(function(photo, id){
                option.attachments.push({filename: 'photo_' + (id + 1) + '.jpg', path: __dirname + '/../static/images/standard/' + photo})
            })
            transporter.sendMail(option, function(error, info){
                data.attempts++
                if(error) {
                    data.sent = false;
                    console.log('Ошибка отправки фотографий на почту' + error);
                } else {
                    data.photos.forEach(function(photo){
                        hideImage(photo, false)
                    })
                    data.sent = true;
                    console.log('Фотографии были успешно отправленны на почту: ' + option.to);
                }
                dbContacts.update({ _id: data._id }, data, {}, function (err, numReplaced) {
                    dbContacts.find({}).exec(function (err, docs) {
                        sendMessage('contacts', docs, -1);
                    });
                });
            });
        }
    }
}

setInterval(function(){
    dbContacts.find({}).exec(function (err, docs) {
        docs.forEach(function(contact){
            sendMail(contact);
        });
    });
}, config.attempts_intrrval || 60e3);


// HTTP
var port = config.port || 3000;
var app = express();
var server = http.createServer(app);
app.use(express.static(__dirname + '/../static'));
server.listen(port, function() {
    console.log('Сервер запущен на : http://localhost:' + port)
});

app.use('/admin', express.static(__dirname + '/../static/admin.html'));


// WS
var ws = new WS.Server({server: server});
var sokets = [];
console.info('Websocket server created');
ws.on('connection', function(ws) {
    var id = ~sokets.lastIndexOf(null)? sokets.lastIndexOf(null):sokets.length;

    ws.onmessage = function(message){
        var message = JSON.parse(message.data);
        var type = message.type;
        var content = message.content;
        if(type == 'form'){
            content.sent = false;
            content.attempts = 0;
            dbContacts.insert(content, function (err, newDoc) { 
                sendMail(newDoc);
                console.log('Пользовательские данные из формы были успешно сохранены в БД')
            });
            if(content.print){
                content.photos.forEach(function(filename){
                    var run = 'rundll32 shimgvw.dll,ImageView_PrintTo "'+(__dirname.split('\\').slice(0, -1).join('\\') + '\\static\\images\\standard\\' + filename)+'" "'+config.printer_name+'"';
                    exec(run, function callback(error, stdout, stderr){
                        if(error || stderr) console.log(error, stderr);
                        else console.log('send to print');
                    });   
                })
            }
        } else if(type == 'login'){
            if(content == config.admin.password) {
                this.admin = true;
                sendMessage('accessed', false, ws.id);
                dbContacts.find({}).exec(function (err, docs) {
                    sendMessage('contacts', docs, ws.id);
                });
            } else {
                sendMessage('access_denied', false, ws.id);
            }
        } else if ((type == 'hide' || type == 'show') && ws.admin) {
            hideImage(content, type=='show')
        }
    };

    ws.onclose = function(msg) {
        console.log('Закрылось WebSocket соединение: #' + this);
        sokets[this] = null;
    }.bind(id);

    console.log('Новое WebSocket соединение: #' + id);
    ws.id = id;
    sokets[id] = ws;
    sendMessage('config', {
        max_select: config.max_select,
        send_to_email: config.send_to_email,
        send_to_print: config.send_to_print,
    }, id);
    dbPhoto.find({}).sort({ photo: 1 }).exec(function (err, docs) {
        sendMessage('images', docs, id);
    });
});


function hideImage(photo, display){
    dbPhoto.update({photo:photo}, {
        photo: photo,
        display:display
    }, {}, function (err, numReplaced){
        sendMessage(display?'show':'hide', photo);
    })
}

function sendMessage(type, content, id) {
    var msg = JSON.stringify({
        type: type,
        content: content || false
    });

    if (id != undefined&& id != -1) {
        sokets[id].send(msg);
    } else {
        sokets.forEach(function(soket){
            if(soket && ((id != -1)||(soket.admin && id == -1))) soket.send(msg);
        });
    }
}

// IMAGE

var frame_path = 'settings/frame.png';

// чтене размера рамки
gm(frame_path).size(function(err, value){
    if(err) return false
    config.image.standard.width = value.width;
    config.image.standard.height = value.height;
})

var count;
function frame(name) {
    var name = images.shift()
    var img = config.image;
    var filename = ('0000' + (++count)).slice(-4)+'.jpg';

    gm(__dirname + '/../' + config.watchpath + '/' + name)
    .resize(img.standard.width, img.standard.height, '!')
    .draw(['image Over 0,0 ' + img.standard.width + ',' + img.standard.height + ' ' + frame_path])
    .quality(img.standard.quality)
    .write(__dirname + '/../static/images/standard/' + filename, function() {
        console.log('Добавлена рамка к ' + filename);
        thumbnail(name, filename);
    });
}

function thumbnail(name, filename){
    var img = config.image
    gm(__dirname + '/../' + config.watchpath + '/' + name)
    .resize(img.thumbnail.width, img.thumbnail.height, '!')
    .quality(img.thumbnail.quality)
    .write(__dirname + '/../static/images/thumbnail/' + filename, function() {
        console.log('Миниатюра ' + filename + ' создана');
    });
}


var images = [];
var watcherTimeout;

var photoWatcher = hound.watch(__dirname + '/../' + config.watchpath);
console.log('Отслеживание фото в папке ' + config.watchpath);

photoWatcher.on('create', function(file, stats) {
    images.push(path.parse(file).base);
    console.log('Найдена новая фотография ' + file);
    clearTimeout(watcherTimeout)
    watcherTimeout = setTimeout(function(){
        images.sort();
        frame();
    }, 5000);
});

var imageWatcher = hound.watch(__dirname + '/../static/images/thumbnail/');
imageWatcher.on('create', function(file, stats) {
    file = path.parse(file).base;
    dbPhoto.insert({photo:file, display:true}, function (err, newDoc) { 
        sendMessage('images', [newDoc]);
        if(images.length)frame();
    });
});
