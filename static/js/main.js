(function(){
    var printFlag = false;
    // WebSocket

    var ws = new WebSocket(location.origin.replace(/^http/, 'ws'));
    ws.addEventListener('open', function(event){
        console.info('Socket open');
    })

    ws.addEventListener('message', function(event){
        var message = JSON.parse(event.data);
        var type = message.type;
        var content = message.content;

        if (type == 'images') {
            content.forEach(function(photo){
                addImage(photo);
            });
        } else if(type == 'hide' || type == 'show') {
            changeImage(type == 'hide', content)
        } else if(type == 'config'){
            max_select = content.max_select;
            if(!content.send_to_email) document.getElementById('email_btn').classList.add('hidden');
            if(!content.send_to_print) document.getElementById('print_btn').classList.add('hidden');
        }else {
            console.warn(type, content);
        }
    });

    ws.addEventListener('close', function(event){
        if (event.wasClean) {
            console.info('Socket clouse conecting');
        } else {
            console.warn('Socket disconected');
        }
    });

    ws.addEventListener('error', function(event){
        console.warn('Socket ERROR:', event.data);
    });

    function sendMesage(type, content){
        ws.send(JSON.stringify({type:type, content:content}));
    }


    // Работа с добавлением фоторграфий на главный экран

    var inputs = document.querySelectorAll('.view.form input, .view.form select, .view.form textarea');
    var images_list = document.getElementById('images_list');
    var select_images = document.getElementById('select_images');
    // var next_btn = document.getElementsByClassName('next_btn');

    function changeImage(n, img) {
        var images = document.querySelector('img[src*="' + img + '"]');
        images.parentElement.classList[n?'add':'remove']('delete');
    }

    function getImage() {
        return document.querySelectorAll('#images_list .select > img');
    }

    var max_select;
    function addImage(photo){
        var img = new Image();
        img.src = '/images/thumbnail/' + photo.photo;
        var li = document.createElement('li');
        li.appendChild(img);
        li.addEventListener('click', function(){
            var images = getImage();
            if(images.length<max_select || this.classList.contains('select')) {
                this.classList.toggle('select');
            }
            document.getElementById('print_btn').classList[getImage().length ? 'remove' : 'add']('disabled');
            document.getElementById('email_btn').classList[getImage().length ? 'remove' : 'add']('disabled');
        }, false);

        images_list.insertBefore(li, images_list.firstChild);

        changeImage(!photo.display, photo.photo);
    }

    /* Проверка фотмы на заполнение */

    var formSbumit = false;

    for(var i = 0; i < inputs.length; i++){
        var removeError =  function(){
            this.classList.remove('error');
            checkForm();
        }
        inputs[i].oninput = removeError;
        inputs[i].onchange = removeError;
    }

    function validateEmail(email) {
        return /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i.test(email);
    }

    function checkForm() {
        var error = false;
        for(var i = 0; i < inputs.length; i++) {
            inputs[i].classList.remove('error');
            if( !(printFlag && inputs[i].parentElement.getAttribute('data-print') == null) &&
                inputs[i].required && (
                (inputs[i].type == 'checkbox' && !inputs[i].checked) ||
                (!inputs[i].value) ||
                (inputs[i].name == 'email' && !validateEmail(inputs[i].value))
            )){
                if(formSbumit) inputs[i].classList.add('error');
                error = true
            }
        }
        document.getElementById('accept_btn').classList[error ? 'add' : 'remove']('disabled');
        return !error;
    }

    /* событие на кнопку далее */
    var next = function(){
        if(!this.classList.contains('disabled')) {
            if(this.id == 'print_btn') {
                printFlag = true;
                document.querySelector('.view.form').classList.add('form_print');
            }
            var images = getImage();
            select_images.innerHTML = '';
            for(var i = 0; i < images.length; i++) {
                select_images.appendChild(images[i].cloneNode());
            }
            document.querySelector('.view.form').classList.add('visible');
            document.querySelector('.view.s_photos').classList.remove('visible');
        }
    }

    document.getElementById('print_btn').addEventListener('click', next);
    document.getElementById('email_btn').addEventListener('click', next);
    document.getElementById('accept_btn').addEventListener('click', function(){
        formSbumit = true;
        if(checkForm()){
            var data = {}
            for(var i = 0; i < inputs.length; i++){
                if(!(inputs[i].type == 'checkbox' && inputs[i].required)){
                    data[inputs[i].name] = inputs[i].type == 'checkbox'? inputs[i].checked : inputs[i].value;
                }
                if(inputs[i].type == 'checkbox'){
                    inputs[i].checked = false;
                }else{
                    inputs[i].value = '';
                }
            }
            var photos = getImage();
            data.photos = [];
            for(var i = 0; i < photos.length; i++) {
                data.photos.push(photos[i].src.split('/').pop());
            }
            if(printFlag)data.print = true;
            console.log(data);
            sendMesage('form', data);
            document.querySelector('.view.form').classList.remove('visible');
            document.querySelector('.view.form_ok').classList.add('visible');
            setTimeout(back, 10000);
        }
    }, false);


    /* Действия на кнопку назад */
    var backBtns = document.querySelectorAll('.logo, .back');
    for(i=0;i<backBtns.length;i++){
        backBtns[i].addEventListener('click', back);
    }
    function back(){
        document.querySelector('.view.form').classList.remove('visible');
        document.querySelector('.view.form').classList.remove('form_print');
        document.querySelector('.view.form_ok').classList.remove('visible');
        document.querySelector('.view.s_photos').classList.add('visible');
        var img = getImage();
        for(var i=0; i<img.length; i++)img[i].parentElement.classList.remove('select');
        document.getElementById('print_btn').classList.add('disabled');
        document.getElementById('email_btn').classList.add('disabled');
        document.getElementById('accept_btn').classList.add('disabled');
        printFlag = false;
    }
})();
