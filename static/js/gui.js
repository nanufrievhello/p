(function(){

// Селектор ввода года в форме
years = document.querySelector('#years');
if(years){
    for(var i = (new Date()).getFullYear(); i>=1900; i--){
        opt = document.createElement('option');
        opt.innerText = i;
        opt.value = i;
        years.appendChild(opt);
    }
}

// Прдстраивание размера экрана отнасительно
// размера шапки при маштабировании окна

function onresize(){
    var header_height = document.querySelector('header').clientHeight;
    page.style.marginTop = header_height+'px';
    page.style.height = window.innerHeight-header_height+'px'
};
onresize();
window.addEventListener('resize', onresize, false);
})();
