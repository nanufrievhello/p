(function(){

    // WebSocket
    var ws = new WebSocket(location.origin.replace(/^http/, 'ws'));

    ws.addEventListener('open', function(event){
        console.info('Socket open');
        if(localStorage['pass']) sendMesage('login', localStorage['pass']);
    });

    var srcToImg = function(item){
        var src = 'images/thumbnail/'+item;
        return '<a href="'+src+'" target="_blank"><img src="'+src+'" alt="'+item+'"></a>';
    }

    ws.addEventListener('message', function(event){
        var message = JSON.parse(event.data);
        var type = message.type;
        var content = message.content;

        if (type == 'access_denied') {
            console.warn('access_denied');
        } else if( type == 'accessed'){
            console.info('accessed');
            document.querySelector('.admin_panel').classList.add('visible');
            loginBtn.style.display = 'none'; 
            if(password.value) localStorage['pass'] = password.value;
            localStorage['date'] = Date.now();
        } else if( type == 'contacts') {
            //sendMailTable.innerHTML = '';
            var line = document.querySelectorAll('#send_mail_table tr');
            for(var i=1; i<line.length; i++)line[i].parentNode.removeChild(line[i]);
            var dataNames = document.querySelectorAll('#send_mail_table th');
            var types = [];
            for (var i=0; i < dataNames.length; i++) types.push(dataNames[i].getAttribute('data-name'));

            var sentCount = content.filter(function(n){return n.sent}).length
            document.querySelector('#mail_sent').innerText = sentCount;
            document.querySelector('#mail_queue').innerText = content.length - sentCount;

            content.forEach(function(contact) {
                var tr = document.createElement('tr');
                for (var i=0; i<types.length; i++) {
                    var td = document.createElement('td');
                    var value = contact[types[i]]
                    value = contact[types[i]];
                    if(typeof value == 'object') value = value.map(srcToImg).join('');
                    if(value === true) value = '+';
                    if(value === false) value = '-';
                    td.innerHTML = value || '-';
                    tr.appendChild(td);
                }
                sendMailTable.appendChild(tr);
            })
        } else if (type == 'images') {
            content.forEach(function(photo){
                addImage(photo);
            });
        }
    });

    ws.addEventListener('close', function(event){
        if (event.wasClean) {
            console.info('Socket clouse conecting');
        } else {
            console.warn('Socket disconected');
        }
    });

    ws.addEventListener('error', function(event){
        console.warn('Socket ERROR:', event.data);
    });

    function sendMesage(type, content){
        ws.send(JSON.stringify({type:type, content:content}));
    }

    /* login */
    if(parseInt(localStorage['date'])+86400e3 < Date.now()) localStorage.clear();
    password = document.getElementById('password');
    loginBtn = document.getElementById('login_btn');
    sendMailTable = document.getElementById('send_mail_table');

    loginBtn.addEventListener('click', function() {
       sendMesage('login', password.value);
    });


    /* login on enter button */

    password.addEventListener('keydown', function(event) {
        if (event.keyCode == 13) loginBtn.click();
    });

    /* Добавление картинок в админке */
    
    function addImage(photo){
        var img = new Image();
        img.src = '/images/thumbnail/' + photo.photo;
        var li = document.createElement('li');
        li.appendChild(img);
        li.onclick = function(){
            this.classList.toggle('hidden')
            sendMesage(this.classList.contains('hidden')? 'hide':'show', this.children[0].src.split('/').pop())
        }
        if(!photo.display) li.classList.add('hidden');
        images_list.insertBefore(li, images_list.firstChild);
    }
})();
